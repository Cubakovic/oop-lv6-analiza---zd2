﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        Random rand = new Random();
        List<string> list = new List<string>();
        int brojPokusaja;
        string RijecIzListe, RijecULabelu;

        public void Reset()
        {
            RijecIzListe = list[rand.Next(0, list.Count - 1)];
            RijecULabelu = new string('*', RijecIzListe.Length);
            label1.Text = RijecULabelu;
            brojPokusaja = 6;
            label2.Text = brojPokusaja.ToString();
        }



        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string line;
            using (System.IO.StreamReader reader = new System.IO.StreamReader
            (@"C:\Users\K-PAX\Desktop\vjesalo.txt"))
            {
                while ((line = reader.ReadLine()) != null)
                {
                    list.Add(line);
                }
                Reset();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Length == 1)
            {
                if (RijecIzListe.Contains(textBox1.Text))
                {
                    string temp_rijecx = RijecIzListe;
                    while (temp_rijecx.Contains(textBox1.Text))
                    {
                        int index = temp_rijecx.IndexOf(textBox1.Text);
                        StringBuilder builder = new StringBuilder

                        (temp_rijecx);

                        builder[index] = '*';
                        temp_rijecx = builder.ToString();
                        StringBuilder builder2 = new StringBuilder

                        (RijecULabelu);

                        builder2[index] = Convert.ToChar(textBox1.Text);
                        RijecULabelu = builder2.ToString();
                    }
                    label1.Text = RijecULabelu;
                    if (RijecULabelu == RijecIzListe)
                    {
                        MessageBox.Show("Pobjeda!");
                        Reset();
                    }
                }
                else
                {
                    brojPokusaja--;
                    label2.Text = brojPokusaja.ToString();
                    if (brojPokusaja <= 0)
                    {
                        MessageBox.Show("Kraj!");
                        Reset();
                    }
                }
            }
            else if (textBox1.Text.Length > 1)
            {
                if (RijecIzListe == textBox1.Text)
                {
                    MessageBox.Show("Pobjeda!");
                    Reset();
                }
                else
                {
                    brojPokusaja--;
                    label2.Text = brojPokusaja.ToString();
                    if (brojPokusaja <= 0)
                    {
                        MessageBox.Show("Kraj!");
                        Reset();
                    }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
